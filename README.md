# Proyecto_JMeter_InfluxDB_Grafana

En este repositorio se coloca el archivo jmx con pruebas con JMeter para la pagina de prueba Blaze Demo, también los pasos para instalar InfluxDB, así como el instalador de Grafana.

1. Para abrir el Plan de prueba para JMeter, una vez abierto el programa seleccionamos Abrir y seleccionamos el archivo jmx.

2. Para instalar InfluxDB es necesario abrir Windows Powershell como administrador e ingresar

wget https://dl.influxdata.com/influxdb/releases/influxdb-1.8.10_windows_amd64.zip -UseBasicParsing -OutFile influxdb-1.8.10_windows_amd64.zip

- Podemos encontrar la carpeta donde se instalo en C:\Program Files\InfluxData\influxdb\.

- Para iniciar el servidor debemos abrir cmd, acceder a la carpeta donde se encuentra Influxdb (Ejemplo: cd C:\Program Files\InfluxData\influxdb\) y ejecutamos influxd.exe.

- Para configurar las bases de datos debemos abrir cmd, acceder a la carpeta donde se encuentra Influxdb (Ejemplo: cd C:\Program Files\InfluxData\influxdb\) y ejecutamos influx.exe.

- Para crear una base de datos en la pantalla de cmd donde abrimos influx.exe, colocamos CREATE DATABASE nombreDeBaseDeDatos.

3. Conectar influx con JMeter.

- En este repositorio en el archivo jmx (Que podemos abrir con JMeter) en el Plan de Pruebas se encuentra el apartado Backend Listener, este se encuentra configurado para una base de datos llamada "testdb", es posible cambiarla.

- Debemos asegurarnos que en Backend Listener Implementation se encuentra seleccionado "org.apache.jmeter.visualizers.backend.influxdb.influxdbBackendListenerCliente".

- En los parametros en influxdbUrl colocamos http://localhost:8086/write?db=nombreDeBaseDeDatos

- En los parametros en application colocamos el nombre de la transacción.

- En los parametros en summaryOnly colocamos "false" sin comillas.

- En los parametros en testTitle podemos colocar el nombre del test.

NOTA: Para escribir los nombres del test o aplicación si necesitamos separar las palabra debemos utilizar "-" sin las comillas.

4. Para utilizar Grafana es necesario descargar e instalarlo en 

Enlace directo: https://dl.grafana.com/oss/release/grafana-9.5.2.windows-amd64.msi

Pagina de descarga: https://grafana.com/grafana/download?pg=get&platform=windows&plcmt=selfmanaged-box1-cta1&edition=oss (Seleccionamos OSS y nuestro sistema operativo).

- Reiniciamos nuestro dispositivo.

- Ingresamos a través de nuestro navegador en http://localhost:3000/.

- Para ingresar colocamos "admin" sin comillas tanto en la cuenta y contraseña, cambiamos la contraseña.

5. Conectar InfluxDB con Grafana.

- Ingresamos en Administration y luego en Data Source.

- Seleccionamos + Add new data source y seleccionamos InfluxDB.

- En el apartado URL colocamos http://localhost:8086.

- En Database colocamos el nombre de la base de datos (nombreDeBaseDeDatos) creada anteriormente con InfluxDB.

- Seleccionamos Save & Test, y si aparece el mensaje "datasource is working. N measurements found", se encuentra configurado correctamente.

6. Colocar el dashboard para JMeter.

- En el menu seleccionamos el apartado Dashboard.

- Seleccionamos la lista desplegable "New" y seleccionamos Import y abrimos el archivo JSON "Apache JMeter Dashboard-1684438437655".

De esta manera podemos ejecutar nuestras pruebas en JMeter y podemos ver los datos obtenidos de las pruebas en el Dashboard que importamos.